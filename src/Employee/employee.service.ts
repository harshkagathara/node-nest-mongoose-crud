import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';
import { Employee } from './employee.model';

@Injectable()

export class EmployeeService {
    constructor(
        @InjectModel('Employee') private readonly EmployeeModel: Model<Employee>,
      ) {}

    async insert(name: string, city: string, dob :string, phone: number) {
        const newData = new this.EmployeeModel({
            name,
            city,
            dob,
            phone
        });
        const result = await newData.save();
        return result.id as string;
    }

    async getAll() {
        const employees = await this.EmployeeModel.find().exec();
        return employees.map(emp =>({
            id: emp.id,
            name: emp.name,
            city: emp.city,
            dob: emp.dob,
            phone: emp.phone,
        }))
    }

    async getSingle(empId: string) {
        const employee = await this.findEmployee(empId);
        return {
            id: employee.id,
            name: employee.name,
            city: employee.city,
            dob: employee.dob,
            phone: employee.phone,
        };
      }


      private async findEmployee(id: string): Promise<Employee> {
        let product;
        try {
          product = await this.EmployeeModel.findById(id).exec();
        } catch (error) {
          throw new NotFoundException('Could not find Employee.');
        }
        if (!product) {
          throw new NotFoundException('Could not find Employee.');
        }
        return product;
      }

      async update(
        empId: string,
        name: string,
        city: string,
        dob: string,
        phone: number,
      ) {
        const updatedEmployee = await this.findEmployee(empId);
        if (name) {
          updatedEmployee.name = name;
        }
        if (dob) {
          updatedEmployee.dob = dob;
        }
        if (city) {
          updatedEmployee.city = city;
        }
        if (phone) {
            updatedEmployee.phone = phone;
          }
        updatedEmployee.save();
      }
    
      async delete(prodId: string) {
        const result = await this.EmployeeModel.deleteOne({_id: prodId}).exec();
      }

}