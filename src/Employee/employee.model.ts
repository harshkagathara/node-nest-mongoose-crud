import  * as mongoose from "mongoose";

export const EmployeeSchema = new mongoose.Schema({
    name : { type : String , required : true },
    city : { type : String , required : true },
    dob : { type : String , required : true },
    phone : { type : Number , required : true },

})

export interface Employee extends mongoose.Document{
    id : string,
    name: string,
    city: string,
    dob: string,
    phone: Number,

}


